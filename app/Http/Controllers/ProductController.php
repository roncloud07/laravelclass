<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\product;

class ProductController extends Controller
{

    public function index()
    {
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->isMethod('get')){
            //menampilkan form
            return View('product.create');
        }else{
            //save ke db
            $input = $request->only('name', 'purcase_price', 'sell_price');
            Product::create($input);

            return redirect('product/create');
        }
    }
}

    