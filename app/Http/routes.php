<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return "<h1>BERHASIL LOGIN!</h1><hr>Bikin logoutnya ....";
});
Route::controller('auth', 'Auth\AuthController');
Route::controller('password', 'Auth\PasswordController');


Route::get('product/index', 'ProductController@index');
Route::get('product/create', 'ProductController@create');
Route::post('product/create', 'ProductController@create');